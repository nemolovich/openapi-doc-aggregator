package fr.nemolovich.apps.openapidoc.aggregator.config;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class AggregatorConstants {
    public static final String DEFAULT_DISPLAY_SERVICE_NAME = "-- openapi-aggregator --";
    public static final String AGGREGATOR_SERVICE_NAME = "${aggregator.display-service-name:#{T(fr.nemolovich.apps.openapidoc.aggregator.config.AggregatorConstants).DEFAULT_DISPLAY_SERVICE_NAME}}";
    public static final String DEFAULT_UPDATE_FREQUENCY = "60000";
    public static final String AGGREGATOR_UPDATE_FREQUENCY = "${aggregator.update-frequency:#{T(fr.nemolovich.apps.openapidoc.aggregator.config.AggregatorConstants).DEFAULT_UPDATE_FREQUENCY}}";
    public static final String DEFAULT_SWAGGER_URL_KEY = "swagger-url";
    public static final String SWAGGER_URL_PROPERTY_KEY = "${aggregator.swagger-url-property:#{T(fr.nemolovich.apps.openapidoc.aggregator.config.AggregatorConstants).DEFAULT_SWAGGER_URL_KEY}}";
    public static final String SWAGGER_VERSIONING_ENABLED = "${aggregator.versioning.enabled:false}";
    public static final String DEFAULT_SWAGGER_VERSIONS_URL_KEY = "versions-url";
    public static final String SWAGGER_VERSIONS_URL_KEY = "${aggregator.versioning.swagger-url-property:#{T(fr.nemolovich.apps.openapidoc.aggregator.config.AggregatorConstants).DEFAULT_SWAGGER_VERSIONS_URL_KEY}}";
    public static final String DEFAULT_SWAGGER_VERSIONS_URL_PATH = "/aggregator/versions";
    public static final String DEFAULT_SERVICES_LIST_URL = "/services";
    public static final String SERVICES_LIST_URL = "${aggregator.services-list-url:#{T(fr.nemolovich.apps.openapidoc.aggregator.config.AggregatorConstants).DEFAULT_SERVICES_LIST_URL}}";
    public static final String MAX_VERSIONS_URL_TRIES = "${aggregator.versioning.max-versions-url-tries:5}";
    public static final String MAX_VERSIONS_URL_TRY_INTERVAL = "${aggregator.versioning.max-versions-url-try-interval:3600}";

}
