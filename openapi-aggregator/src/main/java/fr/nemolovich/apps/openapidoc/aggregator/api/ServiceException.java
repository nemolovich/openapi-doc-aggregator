package fr.nemolovich.apps.openapidoc.aggregator.api;

public class ServiceException extends RuntimeException {
    public ServiceException(final Throwable t) {
        super(t);
    }
}
