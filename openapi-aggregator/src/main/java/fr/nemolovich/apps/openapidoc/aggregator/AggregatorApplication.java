package fr.nemolovich.apps.openapidoc.aggregator;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@EnableDiscoveryClient
@OpenAPIDefinition(info = @Info(title = "${info.app.name}", version = "${info.app.version:}", description = "${info.app.description:}"))
public class AggregatorApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(AggregatorApplication.class);

    public static void main(final String[] args) {
        final SpringApplication app = new SpringApplication(AggregatorApplication.class);

        final Environment env = app.run(args).getEnvironment();

        try {
            final String defaultServerPort = "8080";
            final String protocol = Boolean.TRUE.equals(
                    env.getProperty("server.port.ssl.enabled", Boolean.class, Boolean.FALSE)) ? "https" : "http";
            final String info = String.format(
                    "\n----------------------------------------------------------\n\t"
                            + "Application '%1$s' is running! Access URLs:\n\t" + "Local: \t\t%2$s://localhost:%3$s\n\t"
                            + "External: \t%2$s://%4$s:%3$s\n\t"
                            + "Profile(s): \t%5$s\n----------------------------------------------------------",
                    env.getProperty("spring.application.name", "Documentation aggregator server"), protocol,
                    env.getProperty("server.port", defaultServerPort), InetAddress.getLocalHost().getHostAddress(),
                    Arrays.toString(env.getActiveProfiles()));
            AggregatorApplication.LOGGER.info(info);
        } catch (final UnknownHostException e) {
            AggregatorApplication.LOGGER.error("Cannot resolve localhost", e);
        }
    }
}
