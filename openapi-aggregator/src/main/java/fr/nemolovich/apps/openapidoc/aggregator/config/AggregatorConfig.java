package fr.nemolovich.apps.openapidoc.aggregator.config;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Configuration
@ConfigurationProperties(prefix = "aggregator")
public class AggregatorConfig {
    private List<String> corsUrls;

    public List<String> getCorsUrls() {
        return Optional.ofNullable(this.corsUrls).orElse(Collections.emptyList());
    }
}
