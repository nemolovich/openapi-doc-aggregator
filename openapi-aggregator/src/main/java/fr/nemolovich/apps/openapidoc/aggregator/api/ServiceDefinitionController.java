package fr.nemolovich.apps.openapidoc.aggregator.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.nemolovich.apps.openapidoc.aggregator.config.AggregatorConstants;
import fr.nemolovich.apps.openapidoc.aggregator.config.CustomOpenApiResource;
import io.swagger.v3.core.util.Json;
import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.annotations.Operation;
import java.util.Optional;
import org.springdoc.core.Constants;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceDefinitionController {

    @Autowired
    private CustomOpenApiResource customOpenApiResource;

    @GetMapping(value = Constants.API_DOCS_URL + "/{serviceName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> swaggerDefinition(@PathVariable final String serviceName) {
        final Optional<String> swaggerDefinition = this.customOpenApiResource.getOpenApiDefinition(serviceName);
        return swaggerDefinition.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = Constants.API_DOCS_URL + "/{serviceName}.yaml", produces = Constants.APPLICATION_OPENAPI_YAML)
    public ResponseEntity<String> yamlSwaggerDefinition(@PathVariable final String serviceName) {
        final Optional<String> swaggerDefinition = this.customOpenApiResource.getOpenApiDefinition(serviceName);
        return swaggerDefinition.map(this::convertJSONtoYAML).map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = AggregatorConstants.SERVICES_LIST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String[]> swaggerDefinitions() {
        return ResponseEntity.ok(this.customOpenApiResource.getOpenApiDefinitions().stream()
                .map(GroupedOpenApi::getGroup).sorted(String::compareTo).toArray((String[]::new)));
    }

    @Operation(hidden = true)
    @GetMapping(value = AggregatorConstants.DEFAULT_SWAGGER_VERSIONS_URL_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String[]> getVersions() {
        return ResponseEntity.ok(new String[0]);
    }

    private String convertJSONtoYAML(final String openApiDefinition) {
        try {
            return Yaml.mapper().writeValueAsString(Json.mapper().readTree(openApiDefinition));
        } catch (final JsonProcessingException e) {
            throw new ServiceException(e);
        }
    }
}
