package fr.nemolovich.apps.openapidoc.aggregator.config;

import java.util.List;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SwaggerUiConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class OpenAPIConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(
                HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build()));
    }

    @Bean
    @Primary
    public List<GroupedOpenApi> getOpenApis(final CustomOpenApiResource customOpenApiResource) {
        return customOpenApiResource.getOpenApiDefinitions();
    }

    @Bean
    @Primary
    public CustomOpenApiResource customOpenApiResource(final SwaggerUiConfigProperties swaggerUiConfigProperties) {
        return new CustomOpenApiResource(swaggerUiConfigProperties);
    }

    @Bean
    public WebMvcConfigurer corsConfig() {
        return new WebMvcConfigurer() {

            @Autowired
            private AggregatorConfig aggregatorConfig;

            @Override
            public void addCorsMappings(final CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins(this.aggregatorConfig.getCorsUrls().toArray(new String[0]));
            }
        };
    }
}
