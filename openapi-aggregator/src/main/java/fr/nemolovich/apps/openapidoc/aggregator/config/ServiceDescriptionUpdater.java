package fr.nemolovich.apps.openapidoc.aggregator.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class ServiceDescriptionUpdater {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceDescriptionUpdater.class);

    @Value(AggregatorConstants.SWAGGER_URL_PROPERTY_KEY)
    private String metadataUrlProperty;

    @Value(Constants.API_DOCS_URL)
    private String apiDocUrl;

    @Value(AggregatorConstants.SWAGGER_VERSIONING_ENABLED)
    private boolean versioningEnabled;

    @Value(AggregatorConstants.SWAGGER_VERSIONS_URL_KEY)
    private String metadataVersionsUrlProperty;

    @Value("${eureka.instance.appname}")
    private String appName;

    @Value(AggregatorConstants.MAX_VERSIONS_URL_TRIES)
    private int maxVersionsUrlTries;

    @Value(AggregatorConstants.MAX_VERSIONS_URL_TRY_INTERVAL)
    private long maxVersionsUrlTryInterval;

    @Value(AggregatorConstants.AGGREGATOR_SERVICE_NAME)
    private String displayServiceName;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate template;

    @Autowired
    private CustomOpenApiResource customOpenApiResource;

    private final ConcurrentMap<String, Integer> versionsUrlTries = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, Long> blackListedVersionsUrl = new ConcurrentHashMap<>();

    @Scheduled(fixedDelayString = AggregatorConstants.AGGREGATOR_UPDATE_FREQUENCY)
    public void updateOpenApiDefinitions() {
        ServiceDescriptionUpdater.LOGGER.debug("Updating OpenAPI definitions...");

        this.customOpenApiResource.clearOpenApiDefinitions();
        this.discoveryClient.getServices().forEach(service -> {
            boolean isCurrentService = this.appName.equalsIgnoreCase(service);
            final String serviceName = isCurrentService ? this.displayServiceName : service;

            ServiceDescriptionUpdater.LOGGER.debug("Refresh OpenAPI definition for service {} ", serviceName);

            final List<ServiceInstance> serviceInstances = this.discoveryClient.getInstances(service);

            if (CollectionUtils.isEmpty(serviceInstances)) {
                ServiceDescriptionUpdater.LOGGER.warn("There is no instance available for service {} ", serviceName);
            } else {
                final ServiceInstance instance = serviceInstances.get(0);

                if (this.versioningEnabled && !isCurrentService) {
                    String versionsUrl = this.getVersionsURL(instance);
                    Optional<String[]> versions = this.getVersions(serviceName, versionsUrl);
                    if (versions.isPresent() && versions.get().length > 0) {
                        Arrays.stream(versions.get()).forEach(version -> this.addVersionedServiceDescription(instance, serviceName, version));
                    } else {
                        this.addServiceDescription(instance, serviceName);
                    }
                } else {
                    this.addServiceDescription(instance, serviceName);
                }

            }
        });

        ServiceDescriptionUpdater.LOGGER.debug("OpenAPI definitions updated");
    }

    private void addVersionedServiceDescription(final ServiceInstance instance, final String serviceName,
                                                final String version) {
        final String swaggerURL = this.getVersionedOpenApiURL(instance, version);
        final String versionedServiceName = serviceName.concat(" - ").concat(version);
        final Optional<Object> jsonData = this.getOpenApiDefinitionForAPI(versionedServiceName, swaggerURL);

        if (jsonData.isPresent()) {
            final String content = this.getOpenAPIDefinitionAsJSON(jsonData.get());
            this.customOpenApiResource.addOpenApiDefinition(versionedServiceName, content);
            ServiceDescriptionUpdater.LOGGER.debug("OpenAPI definition updated for service {} ", versionedServiceName);
        } else {
            ServiceDescriptionUpdater.LOGGER.error("Cannot find OpenAPI definition for service {}",
                versionedServiceName);
        }
    }

    private void addServiceDescription(final ServiceInstance instance, final String serviceName) {
        this.addServiceDescription(instance, serviceName, this.getOpenApiURLTryMetadata(instance));
    }

    private void addServiceDescription(final ServiceInstance instance, final String serviceName,
                                       final String swaggerURL) {
        final Optional<Object> jsonData = this.getOpenApiDefinitionForAPI(serviceName, swaggerURL);

        if (jsonData.isPresent()) {
            final String content = this.getOpenAPIDefinitionAsJSON(jsonData.get());
            this.customOpenApiResource.addOpenApiDefinition(serviceName, content);
            ServiceDescriptionUpdater.LOGGER.debug("OpenAPI definition updated for service {} ", serviceName);
        } else {
            final String defaultSwaggerURL = this.getOpenApiURL(instance);
            if (!swaggerURL.equalsIgnoreCase(defaultSwaggerURL)) {
                ServiceDescriptionUpdater.LOGGER.debug("Try with default definition URL {} for service {}",
                    defaultSwaggerURL, serviceName);
                this.addServiceDescription(instance, serviceName, this.getOpenApiURL(instance));
            } else {
                ServiceDescriptionUpdater.LOGGER.error("Cannot find OpenAPI definition for service {}", serviceName);
            }
        }
    }

    private String getVersionedOpenApiURL(final ServiceInstance instance, final String version) {
        return this.getOpenApiURLTryMetadata(instance).replace("{version}", version);
    }

    private String getOpenApiURLTryMetadata(final ServiceInstance instance) {
        return Optional.ofNullable(instance.getMetadata().get(this.metadataUrlProperty))
            .orElse(this.getOpenApiURL(instance));
    }

    private String getOpenApiURL(final ServiceInstance instance) {
        return this.computeURL(instance).concat(this.apiDocUrl);
    }

    private String getVersionsURL(final ServiceInstance instance) {
        return Optional.ofNullable(instance.getMetadata().get(this.metadataVersionsUrlProperty))
            .orElse(this.computeURL(instance).concat(AggregatorConstants.DEFAULT_SWAGGER_VERSIONS_URL_PATH));
    }

    private String computeURL(final ServiceInstance instance) {
        final String serverUrl;
        if (instance instanceof EurekaServiceInstance) {
            serverUrl = ((EurekaServiceInstance) instance).getInstanceInfo().getHomePageUrl();
        } else {
            serverUrl = instance.getUri().toString();
        }
        return serverUrl;
    }

    private Optional<String[]> getVersions(final String serviceName, final String versionsUrl) {
        Optional<String[]> jsonData = Optional.empty();

        if (this.blackListedVersionsUrl.containsKey(serviceName)) {
            final long interval = System.currentTimeMillis() / 1000 - this.blackListedVersionsUrl.get(serviceName);
            if (interval < this.maxVersionsUrlTryInterval) {
                return jsonData;
            } else {
                ServiceDescriptionUpdater.LOGGER.debug(
                    "Remove service {} from black list for versions URL call after {} seconds", serviceName, interval);
                this.blackListedVersionsUrl.remove(serviceName);
            }
        }

        try {
            jsonData = Optional.ofNullable(this.template.getForObject(versionsUrl, String[].class));
        } catch (final RestClientException ex) {
            ServiceDescriptionUpdater.LOGGER.warn("Error while getting service versions for service {}: {}",
                serviceName, ex.getMessage());
            int tries = Optional.ofNullable(this.versionsUrlTries.get(serviceName)).orElse(0) + 1;
            if (tries < this.maxVersionsUrlTries) {
                this.versionsUrlTries.put(serviceName, tries);
            } else {
                this.versionsUrlTries.remove(serviceName);
                ServiceDescriptionUpdater.LOGGER.debug("Black list service {} for versions URL call", serviceName);
                this.blackListedVersionsUrl.put(serviceName, System.currentTimeMillis() / 1000);
            }
        }

        return jsonData;
    }

    private Optional<Object> getOpenApiDefinitionForAPI(final String serviceName, final String url) {
        ServiceDescriptionUpdater.LOGGER.debug("Accessing the OpenAPI definition for Service {} [@URL={}]", serviceName,
            url);
        try {
            return Optional.ofNullable(this.template.getForObject(url, Object.class));
        } catch (final RestClientException ex) {
            ServiceDescriptionUpdater.LOGGER.error("Error while getting service definition for service {}: {}",
                serviceName, ex.getMessage());
        } catch (final IllegalArgumentException ex) {
            ServiceDescriptionUpdater.LOGGER.debug("Invalid service definition URL {} for service {}: {}",
                url, serviceName, ex.getMessage());
        }
        return Optional.empty();
    }

    private String getOpenAPIDefinitionAsJSON(final Object jsonData) {
        try {
            return new ObjectMapper().writeValueAsString(jsonData);
        } catch (final JsonProcessingException e) {
            ServiceDescriptionUpdater.LOGGER.error("Cannot parse OpenAPI definition: {}", e.getMessage());
            return StringUtils.EMPTY;
        }
    }
}
