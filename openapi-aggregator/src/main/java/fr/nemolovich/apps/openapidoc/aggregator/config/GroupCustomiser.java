package fr.nemolovich.apps.openapidoc.aggregator.config;

import io.swagger.v3.core.filter.SpecFilter;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.parser.OpenAPIV3Parser;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.util.AntPathMatcher;

public class GroupCustomiser extends SpecFilter implements OpenApiCustomiser {

    private final List<String> pathsToMatch;

    private final String specLocation;

    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    public GroupCustomiser(final String specLocation, final String... pathsToMatch) {
        this.specLocation = specLocation;
        this.pathsToMatch = Arrays.asList(pathsToMatch);
    }

    @Override
    public void customise(final OpenAPI openApi) {
        final OpenAPI readOpenApi = new OpenAPIV3Parser().read(this.specLocation);

        openApi.setComponents(readOpenApi.getComponents());
        openApi.setPaths(readOpenApi.getPaths());
        openApi.setExtensions(readOpenApi.getExtensions());
        openApi.setInfo(readOpenApi.getInfo());
        openApi.setExternalDocs(readOpenApi.getExternalDocs());
        openApi.setOpenapi(readOpenApi.getOpenapi());
        openApi.setSecurity(readOpenApi.getSecurity());
        openApi.setServers(readOpenApi.getServers());
        openApi.setTags(readOpenApi.getTags());

        openApi.setOpenapi(readOpenApi.getOpenapi());
        openApi.setInfo(readOpenApi.getInfo());
        openApi.setServers(
                readOpenApi.getServers().stream()
                        .map(s -> new Server().url(s.getUrl()).description(readOpenApi.getInfo().getTitle())
                                .extensions(s.getExtensions()).variables(s.getVariables()))
                        .collect(Collectors.toList()));

        openApi.setPaths(readOpenApi.getPaths());
        openApi.getPaths().entrySet().removeIf(path -> !this.isPathToMatch(path.getKey()));

        super.removeBrokenReferenceDefinitions(openApi);
    }

    private boolean isPathToMatch(final String operationPath) {
        return this.pathsToMatch.stream().anyMatch(pattern -> this.antPathMatcher.match(pattern, operationPath));
    }
}
