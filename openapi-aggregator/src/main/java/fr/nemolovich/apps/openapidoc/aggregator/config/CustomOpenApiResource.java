package fr.nemolovich.apps.openapidoc.aggregator.config;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SwaggerUiConfigProperties;

public class CustomOpenApiResource {

    private final SwaggerUiConfigProperties swaggerUiConfigProperties;

    private final ConcurrentHashMap<String, String> serviceDescriptions = new ConcurrentHashMap<>();

    public CustomOpenApiResource(final SwaggerUiConfigProperties swaggerUiConfigProperties) {
        this.swaggerUiConfigProperties = swaggerUiConfigProperties;
        this.updateResources();
    }

    void addOpenApiDefinition(final String serviceName, final String serviceDescription) {
        this.serviceDescriptions.put(serviceName, serviceDescription);
        this.updateResources();
    }

    public Optional<String> getOpenApiDefinition(final String serviceId) {
        return Optional.ofNullable(this.serviceDescriptions.get(serviceId));
    }

    private GroupedOpenApi buildOpenApi(final String apiName, final String url) {
        final String[] pathsToMatch = {"/**"};
        return GroupedOpenApi.builder().setGroup(apiName).pathsToMatch(pathsToMatch)
                .addOpenApiCustomiser(new GroupCustomiser(url, pathsToMatch)).build();
    }

    public List<GroupedOpenApi> getOpenApiDefinitions() {
        return this.serviceDescriptions.entrySet().stream().map(e -> this.buildOpenApi(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    private void updateResources() {
        this.serviceDescriptions.forEach((k, v) -> this.swaggerUiConfigProperties.addGroup(k));
    }

    public void clearOpenApiDefinitions() {
        this.swaggerUiConfigProperties.setUrls(new HashSet<>());
        this.serviceDescriptions.clear();
    }
}
