package fr.nemolovich.apps.openapidoc.aggregator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AggregatorApplicationTestIT {

    @Test
    public void contextLoads() {
        // Try to load Spring Boot context
        Assert.assertTrue(true);
    }

}
