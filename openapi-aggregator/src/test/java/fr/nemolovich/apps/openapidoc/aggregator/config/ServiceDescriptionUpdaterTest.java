package fr.nemolovich.apps.openapidoc.aggregator.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;

class ServiceDescriptionUpdaterTest {
    final ServiceDescriptionUpdater updater = new ServiceDescriptionUpdater();

    @Test
    void testConvert() {

        Assertions.assertEquals("\"{}\"", getOpenAPIDefinitionAsJSON(this.updater, "{}"));
        Assertions.assertEquals("\"{\\\"key\\\":\\\"data\\\"}\"", getOpenAPIDefinitionAsJSON(this.updater, "{\"key\":\"data\"}"));
        Assertions.assertEquals("\"{\\\"key\\\":\\\"data\\\",\\\"sub\\\":{\\\"sub-key\\\":\\\"sub-data\\\"}}\"", getOpenAPIDefinitionAsJSON(this.updater,
            "{\"key\":\"data\",\"sub\":{\"sub-key\":\"sub-data\"}}"));
    }

    private String getOpenAPIDefinitionAsJSON(final ServiceDescriptionUpdater owner, final Object parameter) {
        try {
            Method method = ServiceDescriptionUpdater.class.getDeclaredMethod("getOpenAPIDefinitionAsJSON", Object.class);
            method.setAccessible(true);
            final String result = (String) method.invoke(owner, parameter);
            method.setAccessible(false);
            return result;
        } catch (ReflectiveOperationException e) {
            Assertions.fail("Cannot invoke method");
            return null;
        }
    }
}
