package fr.nemolovich.apps.openapidoc.sample.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${spring.application.name}")
    private String defaultAppName;

    @GetMapping("/")
    public String index() {
        return String.format("Hello! I'm %s",
                Optional.ofNullable(System.getProperty("spring.application.name")).orElse(this.defaultAppName));
    }

}
