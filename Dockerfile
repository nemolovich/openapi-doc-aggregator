FROM openjdk:15-jdk-slim
LABEL maintainer="Brian GOHIER<nemolovich@hotmail.fr>"

RUN apt-get update \
  && apt-get install -y supervisor gettext-base nginx \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ENV EUREKA_JAR="/app/jars/eureka-server.jar"
ENV AGGREGATOR_JAR="/app/jars/openapi-aggregator.jar"
ENV SAMPLE_JAR="/app/jars/sample-service.jar"
ENV SUPERVISOR_LOG="/app/shared/supervisord.log"
ENV SUPERVISOR_PID="/app/shared/supervisord.pid"
ENV SUPERVISOR_SOCKET="/app/shared/supervisor.sock"
ARG NGINX_PORT=8080
ENV NGINX_PORT=${NGINX_PORT}
ENV PROXY_FOLDER="/app/shared/nginx"
ENV DEMO_AGGREGATOR_CONFIG="/app/demo/aggregator.yml"
ENV DEMO_SAMPLE_CONFIG="/app/demo/sample.yml"
ENV MEMORY=256
ENV HOSTNAME="localhost"

EXPOSE ${NGINX_PORT}
EXPOSE 8761
EXPOSE 9090

RUN [ "/bin/bash", "-c", " mkdir -p /app/{scripts,conf,jars,shared/nginx} && chmod -R 777 /app" ]

VOLUME /app/shared

WORKDIR /app

COPY scripts/*.sh /app/scripts/
COPY conf/*.conf /app/conf/
COPY openapi-aggregator/src/main/resources/aggregator.yml /app/demo/
COPY sample-service/src/main/resources/sample.yml /app/demo/

RUN chmod +x /app/scripts/*.sh

COPY eureka-server/target/eureka-server.jar /app/jars/
COPY openapi-aggregator/target/openapi-aggregator.jar /app/jars/
COPY sample-service/target/sample-service.jar /app/jars/

ENTRYPOINT [ "/app/scripts/start.sh" ]
CMD [ "-n", "-e", "-a" ]
