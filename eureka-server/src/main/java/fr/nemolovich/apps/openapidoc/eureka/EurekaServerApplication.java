package fr.nemolovich.apps.openapidoc.eureka;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.core.env.Environment;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(EurekaServerApplication.class);

    public static void main(final String[] args) {
        final SpringApplication app = new SpringApplication(EurekaServerApplication.class);

        final Environment env = app.run(args).getEnvironment();

        try {
            final String defaultServerPort = "8080";
            final String protocol = Boolean.TRUE.equals(
                    env.getProperty("server.port.ssl.enabled", Boolean.class, Boolean.FALSE)) ? "https" : "http";
            final String info = String.format(
                    "\n----------------------------------------------------------\n\t"
                            + "Application '%1$s' is running! Access URLs:\n\t" + "Local: \t\t%2$s://localhost:%3$s\n\t"
                            + "External: \t%2$s://%4$s:%3$s\n\t"
                            + "Profile(s): \t%5$s\n----------------------------------------------------------",
                    env.getProperty("spring.application.name", "Eureka server"), protocol,
                    env.getProperty("server.port", defaultServerPort), InetAddress.getLocalHost().getHostAddress(),
                    Arrays.toString(env.getActiveProfiles()));
            EurekaServerApplication.LOGGER.info(info);
        } catch (final UnknownHostException e) {
            EurekaServerApplication.LOGGER.error("Cannot resolve localhost", e);
        }
    }
}
