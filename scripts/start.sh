#!/bin/bash

DEFAULT_ENABLE_AGGREGATOR=0
DEFAULT_ENABLE_EUREKA=0
DEFAULT_ENABLE_NGINX=0
DEFAULT_SAMPLE_QUANTITY=0
DEFAULT_NGINX_PORT=8080

curr_dir="$(realpath "`pwd`")"
BASE_PATH="${BASE_PATH:-${curr_dir}}"
MAVEN_BASE_PATH="${MAVEN_BASE_PATH:-${BASE_PATH}}"
SUPERVISOR_CONF="${BASE_PATH}/conf/supervisord.conf"
proxy_folder="${PROXY_FOLDER:-${BASE_PATH}/conf/nginx}"
nginx_config="${NGINX_CONF:-${BASE_PATH}/conf/nginx.conf}"
[ ! -z "${NGINX_CONF}" ] && nginx_custom_conf=true || nginx_custom_conf=false

enable_aggregator=${ENABLE_AGGREGATOR:-${DEFAULT_ENABLE_AGGREGATOR}}
enable_eureka=${ENABLE_EUREKA:-${DEFAULT_ENABLE_EUREKA}}
enable_nginx=${ENABLE_NGINX:-${DEFAULT_ENABLE_NGINX}}
sample_quantity=${SAMPLE_QUANTITY:-${DEFAULT_SAMPLE_QUANTITY}}
nginx_port=${NGINX_PORT:-${DEFAULT_NGINX_PORT}}

function usage()
{
  echo -e "\e[1mUsage\e[0m: \e[0;97m$0\e[0m [-h] [-v,-a,-e,-n,-q <QUANTITY>,-r]
  \e[1mOPTIONS\e[0m:
    \e[0;97m-h|--help\e[0m                        Display this help.
    \e[0;97m-v|--verbose\e[0m                     Enable verbose mode.
    \e[0;97m-a|--aggregator\e[0m                  Enable documentations aggregator server (port \e[0;96m9090\e[0m).
    \e[0;97m-e|--eureka\e[0m                      Enable eureka server (port \e[0;96m8761\e[0m).
    \e[0;97m-n|--nginx\e[0m                       Enable NGINX server (port \e[0;96m\${NGINX_PORT}\e[0m).
    \e[0;97m-p|--proxy-folder <PATH>\e[0m         Path to folder containing additional NGINX configs.
    \e[0;97m-q|--sample-quantity <QUANTITY>\e[0m  Number of sample-service servers to launch (default=\e[0;96m0\e[0m, max=\e[0;96m9\e[0m)
    \e[0;97m-r|--rebuild\e[0m                     Rebuild Maven projects.
  \e[1mENVIRONMENT VARIABLES\e[0m:
    Environment variables are overrides by command options.
    \e[0001mNAME\e[0m                     \e[0001mDEFAULT VALUE\e[0m                          \e[0001mDESRIPTION\e[0m
    \e[0;97mBASE_PATH\e[0m                \e[0000m'pwd'\e[0m                                  The base path to use.
    \e[0;97mSUPERVISOR_SOCKET\e[0m        \e[0000m/tmp/supervisor.sock\e[0m                   The socket file location for Supervisor.
    \e[0;97mSUPERVISOR_LOG\e[0m           \e[000m'\${BASE_PATH}'/supervisord.log\e[0m         The log file to use for Supervisor.
    \e[0;97mSUPERVISOR_CONF\e[0m          \e[000m'\${BASE_PATH}'/conf/supervisord.conf\e[0m   The Supervisor config file to use.
    \e[0;97mNGINX_CONF\e[0m               \e[000m'\${BASE_PATH}'/conf/nginx.conf\e[0m         The NGINX config file to use.
    \e[0;97mNGINX_PORT\e[0m               \e[0000m8080\e[0m                                   The NGINX listening port.
    \e[0;97mENABLE_AGGREGATOR\e[0m        \e[0000m0\e[0m                                      Set to \e[0;96m1\e[0m to enable documentations aggregator server, \e[0;96m0\e[0m to disable.
    \e[0;97mENABLE_EUREKA\e[0m            \e[0000m0\e[0m                                      Set to \e[0;96m1\e[0m to enable eureka server, \e[0;96m0\e[0m to disable.
    \e[0;97mENABLE_NGINX\e[0m             \e[0000m0\e[0m                                      Set to \e[0;96m1\e[0m to enable nginx server, \e[0;96m0\e[0m to disable.
    \e[0;97mSAMPLE_QUANTITY\e[0m          \e[0000m0\e[0m                                      Set the number of sample-service servers to launch (max=\e[0;96m9\e[0m).
    \e[0;97mPROXY_FOLDER\e[0m             \e[000m'\${BASE_PATH}'/conf/nginx\e[0m              Path to folder containing additional NGINX configs.
    \e[0;97mMAVEN_BASE_PATH\e[0m          \e[000m'\${BASE_PATH}'\e[0m                         The Maven base path to build projets.
    \e[0;97mMEMORY\e[0m                   \e[0000m64\e[0m                                     The Java memory to use to run JARs.
    \e[0;97mHOSTNAME\e[0m                 \e[0000mlocalhost\e[0m                              The name resolution of host.
    \e[0;97mEUREKA_SERVER_NAME\e[0m       \e[000m'\${HOSTNAME}'\e[0m                          The name resolution of host for Eureka server.
    \e[0;97mAGGREGATOR_SERVER_NAME\e[0m   \e[000m'\${HOSTNAME}'\e[0m                          The name resolution of host for Aggregator server.
"
}

HELPED=false
VERBOSE=false
REBUILD=false

while [ ! -z "$1" ] ; do
  case $1 in
    -h|--help)
      HELPED=true
      usage
      ;;
    -v|--verbose)
      VERBOSE=true
      ;;
    -a|--aggregator)
      enable_aggregator=1
      ;;
    -e|--eureka)
      enable_eureka=1
      ;;
    -n|--nginx)
      enable_nginx=1
      ;;
    -p|--proxy-folder)
      shift
      folder=$1
      if [ -z "${folder}" -o -r "${folder}" ] ; then
        echo -e "\e[0;91mERROR\e[0m: '${folder}' invalid path. Please specify a valid folder path for NGINX configs." 1>&2
        exit 1
      fi
      proxy_folder=${folder}
      ;;
    -q|--sample-quantity)
      shift
      nb=$1
      number_re='^[0-9]$'
      if ! [[ "${nb}" =~ ${number_re} ]] ; then
        echo -e "\e[0;91mERROR\e[0m: '${nb}' invalid number. Please use number between \e[0;96m0\e[0m and \e[0;96m9\e[0m to specify the number of sample-services to use." 1>&2
        exit 2
      fi
      sample_quantity=${nb}
      ;;
    -r|--rebuild)
      REBUILD=true
      ;;
    *)
      ;;
  esac
  shift
done

if ${HELPED} ; then
  exit 0
fi

INIT_PROXY_FOLDER="${proxy_folder}"
PROXY_FOLDER="${INIT_PROXY_FOLDER}"

mkdir -p "${INIT_PROXY_FOLDER}"

export SUPERVISOR_LOG="${SUPERVISOR_LOG:-${BASE_PATH}/supervisord.log}"
export SUPERVISOR_PID="${SUPERVISOR_PID:-/tmp/supervisord.pid}"
export SUPERVISOR_SOCKET="${SUPERVISOR_SOCKET:-/tmp/supervisor.sock}"

echo "${OS}" | grep -iq "windows" && WINDOWS_OS=true || WINDOWS_OS=false

DEFAULT_EUREKA_JAR="${BASE_PATH}/eureka-server/target/eureka-server.jar"
DEFAULT_AGGREGATOR_JAR="${BASE_PATH}/openapi-aggregator/target/openapi-aggregator.jar"
DEFAULT_SAMPLE_JAR="${BASE_PATH}/sample-service/target/sample-service.jar"

EUREKA_JAR="${EUREKA_JAR:-${DEFAULT_EUREKA_JAR}}"
AGGREGATOR_JAR="${AGGREGATOR_JAR:-${DEFAULT_AGGREGATOR_JAR}}"
SAMPLE_JAR="${SAMPLE_JAR:-${DEFAULT_SAMPLE_JAR}}"

DEFAULT_DEMO_AGGREGATOR_CONFIG="${BASE_PATH}/openapi-aggregator/src/main/resources/aggregator.yml"
DEFAULT_DEMO_SAMPLE_CONFIG="${BASE_PATH}/sample-service/src/main/resources/sample.yml"

DEMO_AGGREGATOR_CONFIG="${DEMO_AGGREGATOR_CONFIG:-${DEFAULT_DEMO_AGGREGATOR_CONFIG}}"
DEMO_SAMPLE_CONFIG="${DEMO_SAMPLE_CONFIG:-${DEFAULT_DEMO_SAMPLE_CONFIG}}"


if [ ${enable_nginx} -eq 1 ] ; then
  if [ ${enable_aggregator} -eq 1 -a -z "${AGGREGATOR_CONFIG}" ] ; then
    ${VERBOSE} && echo -e "\e[0;94mDEBUG\e[0m: NGINX is enabled: default config for aggregator is overrided to allow CORS"
    AGGREGATOR_CONFIG="${DEMO_AGGREGATOR_CONFIG}"
  fi
  if [ ${sample_quantity} -gt 0 -a -z "${SAMPLE_CONFIG}" ] ; then
    ${VERBOSE} && echo -e "\e[0;94mDEBUG\e[0m: NGINX is enabled: default config for sample service is overrided to allow CORS"
    SAMPLE_CONFIG="${DEMO_SAMPLE_CONFIG}"
  fi
fi

if [ ! -f "${EUREKA_JAR}" -o ! -f "${AGGREGATOR_JAR}" -o ! -f "${SAMPLE_JAR}" ] ; then
  ${VERBOSE} && echo -e "\e[0;94mDEBUG\e[0m: Some JARs are missing. Need to rebuild."
  REBUILD=true
fi

if ${WINDOWS_OS} ; then
  ${VERBOSE} && echo -e "\e[0;94mDEBUG\e[0m: Windows OS detected. Converting paths."
  EUREKA_JAR="$(cygpath -w "${EUREKA_JAR}")"
  AGGREGATOR_JAR="$(cygpath -w "${AGGREGATOR_JAR}")"
  SAMPLE_JAR="$(cygpath -w "${SAMPLE_JAR}")"
  [ ! -z "${EUREKA_CONFIG}" ] && EUREKA_CONFIG="/$(cygpath -w "${EUREKA_CONFIG}" | sed 's!\\!\\\\!g')"
  [ ! -z "${AGGREGATOR_CONFIG}" ] && AGGREGATOR_CONFIG="/$(cygpath -w "${AGGREGATOR_CONFIG}" | sed 's!\\!\\\\!g')"
  [ ! -z "${SAMPLE_CONFIG}" ] && SAMPLE_CONFIG="/$(cygpath -w "${SAMPLE_CONFIG}" | sed 's!\\!\\\\!g')"
  MAVEN_BASE_PATH="$(cygpath -w "${MAVEN_BASE_PATH}")"
fi

NGINX_DEFAULT_SAMPLES_CONF="${BASE_PATH}/conf/samples.conf"
NGINX_SAMPLES_CONF="${PROXY_FOLDER}/samples.conf"

if [ ! -f "${NGINX_SAMPLES_CONF}" -a ${sample_quantity} -gt 0 ] ; then
  ${VERBOSE} && echo -e "\e[0;94mDEBUG\e[0m: Copying samples NGINX config into '${INIT_PROXY_FOLDER}'."
  cp -rp "${NGINX_DEFAULT_SAMPLES_CONF}" "${NGINX_SAMPLES_CONF}"
fi

export BASE_PATH="${BASE_PATH}"
export EUREKA_SCRIPT="${BASE_PATH}/scripts/run_eureka.sh"
export AGGREGATOR_SCRIPT="${BASE_PATH}/scripts/run_aggregator.sh"
export SAMPLE_SCRIPT="${BASE_PATH}/scripts/run_sample.sh"
export RUN_JAR_SCRIPT="${BASE_PATH}/scripts/run_jar.sh"
export NGINX_TEMPLATE="${BASE_PATH}/conf/nginx.template.conf"
export NGINX_CONF="${nginx_config}"
export NGINX_BIN="$(which nginx)"
export NGINX_PORT=${nginx_port}
export PROXY_FOLDER="${PROXY_FOLDER}"

export EUREKA_JAR="${EUREKA_JAR}"
export AGGREGATOR_JAR="${AGGREGATOR_JAR}"
export SAMPLE_JAR="${SAMPLE_JAR}"

export EUREKA_CONFIG="${EUREKA_CONFIG}"
export AGGREGATOR_CONFIG="${AGGREGATOR_CONFIG}"
export SAMPLE_CONFIG="${SAMPLE_CONFIG}"

export ENABLE_AGGREGATOR=${enable_aggregator}
export ENABLE_EUREKA=${enable_eureka}
export ENABLE_NGINX=${enable_nginx}
export SAMPLE_QUANTITY=${sample_quantity}

export SERVER_NAME="${HOSTNAME:-localhost}"
export EUREKA_SERVER_NAME="${EUREKA_SERVER_NAME:-${SERVER_NAME}}"
export AGGREGATOR_SERVER_NAME="${AGGREGATOR_SERVER_NAME:-${SERVER_NAME}}"

if ${REBUILD} ; then
  cmd="mvn clean package -f ${MAVEN_BASE_PATH}/pom.xml -DskipTests"
  echo -e "\e[0;97mINFO\e[0m: Rebuild maven projects..."
  ${VERBOSE} && echo -e "\e[0;94mDEBUG\e[0m: cmd='${cmd}'"
  ${VERBOSE} && ${cmd} || ${cmd} 2>/dev/null 1>&2
  ret_code=$?
  if [ ${ret_code} -ne 0 ] ; then
    echo -e "\e[0;91mERROR\e[0m: Failed to rebuild maven projetcs." 1>&2
    exit 3
  else
    echo -e "\e[0;92mSUCCESS\e[0m: Maven projetcs rebuilt."
  fi
fi

if ! ${nginx_custom_conf} ; then
  echo -e "\e[0;97mINFO\e[0m: Evaluate environment variables into NGINX config file."
  envsubst '${PROXY_FOLDER} ${NGINX_PORT}' < "${NGINX_TEMPLATE}" > "${NGINX_CONF}"
fi

${VERBOSE} && echo -e "\e[0;97mINFO\e[0m: Starting processes with following parameters:
  BASE_PATH:                \e[0;96m${BASE_PATH}\e[0m
  SUPERVISOR_SOCKET:        \e[0;96m${SUPERVISOR_SOCKET}\e[0m
  SUPERVISOR_LOG:           \e[0;96m${SUPERVISOR_LOG}\e[0m
  SUPERVISOR_CONF:          \e[0;96m${SUPERVISOR_CONF}\e[0m
  NGINX_CONF:               \e[0;96m${NGINX_CONF}\e[0m
  NGINX_BIN:                \e[0;96m${NGINX_BIN}\e[0m
  NGINX_PORT:               \e[0;96m${NGINX_PORT}\e[0m
  PROXY_FOLDER:             \e[0;96m${INIT_PROXY_FOLDER}\e[0m
  ENABLE_AGGREGATOR:        \e[0;96m${ENABLE_AGGREGATOR}\e[0m
  ENABLE_EUREKA:            \e[0;96m${ENABLE_EUREKA}\e[0m
  ENABLE_NGINX:             \e[0;96m${ENABLE_NGINX}\e[0m
  SAMPLE_QUANTITY:          \e[0;96m${SAMPLE_QUANTITY}\e[0m
  EUREKA_CONFIG:            \e[0;96m$([ -z "${EUREKA_CONFIG}" ] && echo "(default)" || echo "${EUREKA_CONFIG}")\e[0m
  AGGREGATOR_CONFIG:        \e[0;96m$([ -z "${AGGREGATOR_CONFIG}" ] && echo "(default)" || echo "${AGGREGATOR_CONFIG}")\e[0m
  SAMPLE_CONFIG:            \e[0;96m$([ -z "${SAMPLE_CONFIG}" ] && echo "(default)" || echo "${SAMPLE_CONFIG}")\e[0m
  SERVER_NAME:              \e[0;96m${SERVER_NAME}\e[0m
  EUREKA_SERVER_NAME:       \e[0;96m${EUREKA_SERVER_NAME}\e[0m
  AGGREGATOR_SERVER_NAME:   \e[0;96m${AGGREGATOR_SERVER_NAME}\e[0m
"

supervisord -c "${SUPERVISOR_CONF}"
