#!/bin/bash

MEMORY=${MEMORY:-64}
MEMORY_UNIT=${MEMORY_UNIT:-m}

D_MEMORY=$[MEMORY/2]${MEMORY_UNIT}
M_MEMORY=$[MEMORY+1]${MEMORY_UNIT}
F_MEMORY=${MEMORY}${MEMORY_UNIT}

jar="$1"
shift

java -XX:+UseParallelGC -XX:NewSize=${F_MEMORY} -XX:MaxNewSize=${M_MEMORY} \
  -Xms${D_MEMORY} -Xmx${M_MEMORY} -XX:MaxMetaspaceSize=${M_MEMORY} \
  $* -jar "${jar}"
