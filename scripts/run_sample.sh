#!/bin/bash

proc_num=$1
app_number=$[proc_num+1]
port="808${app_number}"
path=/sample${app_number}
name="sample-app${app_number}"

args=""

if [ ! -z "$2" ] ; then
  args="-Dspring.config.location=file://$2"
fi

trap "kill -- -$$" EXIT

"${RUN_JAR_SCRIPT}" "${SAMPLE_JAR}" ${args} -Dserver.port=${port} \
  -Dserver.servlet.context-path=${path} -Dspring.application.name="${name}"
