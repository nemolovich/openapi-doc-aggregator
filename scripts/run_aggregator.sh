#!/bin/bash

args=""

if [ ! -z "$1" ] ; then
  args="-Dspring.config.location=file://$1"
fi

trap "kill -- -$$" EXIT

"${RUN_JAR_SCRIPT}" "${AGGREGATOR_JAR}" ${args}
